MyKMAfinder
===================

This project documents the MyKMAfinder service


Documentation
=============

## What is it?

The MyKMAfinder service contains one python script *mykmafinder.py* which is the script of the latest
version of the MyKMAfinder service. The method is used to find the best match (species identification) to the reads in one
or more *fastq* files  or one *fasta* file in a (kmer) database produced using the KMA program. The program can create the kmer database from an input fasta file or a batch file including the filenames of fasta files. With MyKMAfinder, two main mappings with KMA are provided, one that performs full alignment (mode = "full") and one "sparse" (mode = "sparse"), where the alignments are skipped.
The method outputs the best matches determined. If full mode is selected, a results file with the alingments is also produced.

## Content of the repository
1. mykmafinder.py      - the program
2. README.md
3. Dockerfile   - dockerfile for building the mykmafinder docker container

## Installation

Setting up MyKMAfinder program
```bash
# Go to wanted location for resfinder
cd /path/to/some/dir
# Clone and enter the MyKMAfinder directory
git clone https://bitbucket.org/genomicepidemiology/mykmafinder.git
cd mykmafinder
```

Build Docker image from Dockerfile
```bash
# Build container
docker build -t mykmafinder .
# Run test
docker run --rm -it \
       --entrypoint=/test/test.sh mykmafinder
```

## Usage
First save the directory where your database is/will be created. As a note, if you want to create a database using MyKMAfinder, the newly constructed database will be stored in the directory where the fasta file or the text file listing the fasta files are stored.

```bash
# Go to the directory where you have stored/want to store the MyKMAfinder database - this should be the directory of your fasta file/ text file listing the fasta files
cd /path/to/database/dir
MyKMAfinder_DB=$(pwd)
```
The program can be invoked with the -h option to get help and more information of the service.

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -h
```
MyKMAfinder program provides the option to create your own custom databases, either by providing a single fasta file or multiple fasta files. The latter one is achieved by providing a text file listing the names of fasta files. This batch file should have a content such as:
```
file1.fasta
file2.fasta
file3.fasta
...
```
**NOTE:** It is important that the text batch file listing the fasta filenames is in the exact same directory where all the fasta files are. This is the directory were the databases will be stored.

There is also an option to define kmer size and the prefix used to create the database. In case the kmer size or prefix arguments are not defined, the default values are selected (prefix = '-', i.e. no prefix and Kmersize = 16). In addition, there is an option to set the name of the database. As long as you have created the desired database, you can then run the program with this database, without needing to re-create it again. Below are several examples on how to run MyKMAfinder, in different cases.

MyKMAfinder, can be run in two modes, one is full alignment and the other one is sparse, where the alignments are skipped. By providing the `-sm` option, the sparse mode is selected.

In the example instructions below, the way to run MyKMAfinder in sparse mode is presented. By excluding the `-sm` argument as well as the prefix argument (`-prf [PREFIX]`), you can run the program in full mode.

### Create a database from a single fasta file and run MyKMAfinder

One way to run the program is to go inside the folder where your input files are:

```bash 
cd /path/to/input/file
```
And then run:

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i [INPUTFILE] -o . -dbn [DATABASE_NAME] -dbf /database/[FASTA_FILENAME] -prf [PREFIX] -ks [KMERSIZE] -sm
```

This way your results files will be placed inside your input files directory. In case you omit the "-o" argument
a folder named "output" will be created in you input files directory with the results of the analysis. 

If you would like to save your results in a folder with specific name, you can run the above command with:

```bash
-o ./[FOLDERNAME]
```
and a folder with a specified name , holding the results of the analysis will be created inside the input file directory.


Below is an example of running the above command using a database, with prefix=ATG and kmersize=21

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i myfile.fastq.gz -o ./myresults -dbn mytestdb -dbf /database/test.fna -prf ATG -ks 21 -sm
```

Of course you can define the full path to your input file(s) and run the above command as:
```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v /full/path/to/input/files:/workdir \
       mykmafinder -i [INPUTFILE] -o . -dbn [DATABASE_NAME] -dbf /database/[FASTA_FILENAME] -prf [PREFIX] -ks [KMERSIZE] -sm
```

The ```-prf [PREFIX] -ks [KMERSIZE]``` can be excluded, and default values of prefix=- (no prefix) and kmersize=16 will be used instead.

### Create a database from a batch file listing multiple fasta files and run MyKMAfinder

One way to run the program is to go inside the folder where your input files are:

```bash 
cd /path/to/input/file
```
And then run:

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i [INPUTFILE] -o . -dbn [DATABASE_NAME] -dbf_batch /database/[BATCH_FILENAME] -prf [PREFIX] -ks [KMERSIZE] -sm
```

This way your results files will be placed inside your input files directory. In case you omit the "-o" argument
a folder named "output" will be created in you input files directory with the results of the analysis. 

If you would like to save your results in a folder with specific name, you can run the above command with:

```bash
-o ./[FOLDERNAME]
```
and a folder with a specified name , holding the results of the analysis will be created inside the input file directory.

Below is an example of running the above command using a database, with prefix=ATG and kmersize=21

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i myfile.fastq.gz -o ./myresults -dbn mytestdb -dbf_batch /database/test_batch.txt -prf ATG -ks 21 -sm
```

Of course you can define the full path to your input file(s) and run the above command as:
```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v /full/path/to/input/files:/workdir \
       mykmafinder -i [INPUTFILE] -o . -dbn [DATABASE_NAME] -dbf_batch /database/[BATCH_FILENAME] -prf [PREFIX] -ks [KMERSIZE] -sm
```
### Run MyKMAfinder with an already constructed custom database

One way to run the program is to go inside the folder where your input files are:

```bash 
cd /path/to/input/file
```
And then run:

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i [INPUTFILE] -o . -db /database/[DATABASE_NAME] -sm
```
**Note**: if you have constructed the database selecting a prefix, the database name will include the prefix selected to construct the database. See example below.

This way your results files will be placed inside your input files directory. In case you omit the "-o" argument
a folder named "output" will be created in you input files directory with the results of the analysis. 

If you would like to save your results in a folder with specific name, you can run the above command with:

```bash
-o ./[FOLDERNAME]
```
and a folder with a specified name , holding the results of the analysis will be created inside the input file directory.


Below is an example of running the above command using a database, created with prefix=ATG

```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v $(pwd):/workdir \
       mykmafinder -i myfile.fastq.gz -o ./myresults -db /database/mytestdb.ATG -sm
```

Of course you can define the full path to your input file(s) and run the above command as:
```bash
docker run --rm -it \
        -v $MyKMAfinder_DB:/database \
        -v /full/path/to/input/files:/workdir \
       mykmafinder -i [INPUTFILE] -o . -db /database/[DATABASE_NAME] -sm
```

When running the docker file you have to mount 2 required directories: 

1. MyKMAfinder_DB (MyKMAfinder database directory), which includes the databases created with MyKMAfinder
2. An output/input folder from where the input file can be reached and an output files can be saved. 

Here you can mount either the current working directory (using $pwd) which points to your input file(s) 
or specify a full path to the directory of your input file(s) and use this as the output directory.

-i    INPUTFILE	   input file(s) (fasta or fastq)

-db      DATABASE_PATH/DATABASE_NAME      MyKMAfinder database with path

-dbf     DATABASE_PATH/FASTA_FILENAME     fasta file with path from which the database will be constructed

-dbf_batch     DATABASE_PATH/BATCH_FILENAME     text file with its path, the file includes the names of fasta files to be used to construct the database

-dbn     DATABASE_NAME     name of database

-o    OUTDIR	   outpur directory relative to pwd or speficied directory of input files

-prf  PREFIX   prefix used when creating the database, default='-', i.e. no prefix used

-ks   KMERSIZE kmersize used when creating the database, default=16

## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
http://cge.cbs.dtu.dk/services/MyKMAfinder/

Citation
=======

When using the method please cite:

Benchmarking of Methods for Genomic Taxonomy. Larsen MV, Cosentino S,
Lukjancenko O, Saputra D, Rasmussen S, Hasman H, Sicheritz-Pontén T,
Aarestrup FM, Ussery DW, Lund O. J Clin Microbiol. 2014 Feb 26.
[Epub ahead of print]

Rapid whole genome sequencing for the detection and characterization of
microorganisms directly from clinical samples. Hasman H, Saputra D,
Sicheritz-Ponten T, Lund O, Svendsen CA, Frimodt-Møller N, Aarestrup FM.
J Clin Microbiol.  2014 Jan;52(1):139-46.

Rapid and precise alignment of raw reads against redundant databases with KMA Philip T.L.C. Clausen, Frank M. Aarestrup, Ole Lund.

License
=======


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
