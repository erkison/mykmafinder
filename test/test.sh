#!/bin/bash 
#kma_index -i /database/pseudomonas/pseudomonas.fna -o /database/pseudomonas/pseudomonas.ATG -Sparse ATG &> /database/pseudomonas/pseudomonas.log
 
mykmafinder.py -i /test/test.fastq.gz -o /results -dbn pseudomonas -dbf /database/pseudomonas/pseudomonas.fna -sm -prf ATG

file=/results/results.spa
DIFF=$(diff $file /test/test_results.txt)
if [ "$DIFF" == "" ] && [ -s $file ] ;
   then     
   echo "TEST SUCCEEDED"; 
else
   echo "TEST FAILED";
fi
